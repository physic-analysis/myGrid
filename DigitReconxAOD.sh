#!/bin/sh

# setupATLAS; lsetup git python; asetup 21.0.68,Athena

pathena --trf " Reco_tf.py 
  --maxEvents '10' \
  --digiSteeringConf 'StandardSignalOnlyTruth' \
  --conditionsTag 'default:OFLCOND-MC16-SDR-23' \
  --geometryVersion 'default:ATLAS-R2-2016-01-00-01' \
  --autoConfiguration 'everything' \
  --steering 'doRAWtoALL' \
  --valid 'True' \
  --postInclude 'default:PyJobTransforms/UseFrontier.py' \
  --postExec 'all:CfgMgr.MessageSvc().setError+=[\"HepMcParticleLink\"]' \
  'RAWtoALL:from InDetPrepRawDataToxAOD.InDetPrepRawDataToxAODConf import PixelPrepDataToxAOD;xAOD_PixelPrepDataToxAOD=PixelPrepDataToxAOD(name=\"xAOD_PixelPrepDataToxAOD\");xAOD_PixelPrepDataToxAOD.WriteSiHits=True;' \
  'HITtoRDO:outStream=topSequence.StreamRDO;outStream.ItemList+=[\"SiHitCollection#PixelHits\"];outStream.ItemList+=[\"SiHitCollection#SCT_Hits\"];' \
  'RAWtoALL:outStream=topSequence.StreamESD;outStream.ItemList+=[\"SiHitCollection#PixelHits\"];outStream.ItemList+=[\"SiHitCollection#SCT_Hits\"];' \
  'RAWtoALL:IDTRKVALIDStream.AddItem(\"xAOD::TauTrackContainer#TauTracks\"); IDTRKVALIDStream.AddItem(\"xAOD::TauTrackAuxContainer#TauTracksAux.\"); IDTRKVALIDStream.AddItem(\"xAOD::TauJetContainer#TauJets\");IDTRKVALIDStream.AddItem(\"xAOD::TauJetAuxContainer#TauJetsAux.\"); IDTRKVALIDStream.AddItem(\"xAOD::JetContainer#AntiKt4EMTopoJets\"); IDTRKVALIDStream.AddItem(\"xAOD::JetAuxContainer#AntiKt4EMTopoJetsAux.\"); IDTRKVALIDStream.AddItem(\"xAOD::JetContainer#AntiKt4LCTopoJets\"); IDTRKVALIDStream.AddItem(\"xAOD::JetAuxContainer#AntiKt4LCTopoJetsAux.\"); IDTRKVALIDStream.AddItem(\"xAOD::JetContainer#AntiKt2PV0TrackJets\"); IDTRKVALIDStream.AddItem(\"xAOD::JetAuxContainer#AntiKt2PV0TrackJetsAux.\"); IDTRKVALIDStream.AddItem(\"xAOD::JetContainer#AntiKt3PV0TrackJets\"); IDTRKVALIDStream.AddItem(\"xAOD::JetAuxContainer#AntiKt3PV0TrackJetsAux.\"); IDTRKVALIDStream.AddItem(\"xAOD::BTaggingContainer#BTagging_AntiKt4EMTopo\");IDTRKVALIDStream.AddItem(\"xAOD::BTaggingAuxContainer#BTagging_AntiKt4EMTopoAux.\"); IDTRKVALIDStream.AddItem(\"xAOD::BTaggingContainer#BTagging_AntiKt4LCTopo\"); IDTRKVALIDStream.AddItem(\"xAOD::BTaggingAuxContainer#BTagging_AntiKt4LCTopoAux.\"); IDTRKVALIDStream.AddItem(\"xAOD::BTaggingContainer#BTagging_AntiKt2Track\");IDTRKVALIDStream.AddItem(\"xAOD::BTaggingAuxContainer#BTagging_AntiKt2TrackAux.\"); IDTRKVALIDStream.AddItem(\"xAOD::BTaggingContainer#BTagging_AntiKt3Track\"); IDTRKVALIDStream.AddItem(\"xAOD::BTaggingAuxContainer#BTagging_AntiKt3TrackAux.\");' \
  --preExec 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from LArDigitization.LArDigitizationFlags import jobproperties;jobproperties.LArDigitizationFlags.useEmecIwHighGain.set_Value_and_Lock(False);rec.doTrigger.set_Value_and_Lock(False);' \
  'all:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doSlimming.set_Value_and_Lock(False)' \
  'all:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock(\"AODFULL\");' \
  'HITtoRDO:userRunLumiOverride={\"run\":300000, \"startmu\":45.0, \"endmu\":88.0, \"stepmu\":1.0, \"startlb\":1, \"timestamp\":1500000000};ScaleTaskLength=0.4;' \
  'RAWtoALL:from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags;InDetDxAODFlags.DRAWZSelection.set_Value_and_Lock(False);InDetDxAODFlags.DumpPixelInfo
.set_Value_and_Lock(True);InDetDxAODFlags.DumpPixelRdoInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpPixelNNInfo.set_Value_and_Lock(True);InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(False);InDetDxAODFlags.DumpTrtInfo.set_Value_and_Lock(False);InDetDxAODFlags.ThinHitsOnTrack.set_Value_and_Lock(False);InDetDxAODFlags.DumpLArCollisionTime.set_Value_and_Lock(False);' \
  --inputHITSFile '%IN' \
  --outputESDFile '%OUT.ESD.pool.root' \
  --outputDAOD_IDTRKVALIDFile '%OUT.xAOD.pool.root'" \
  --inDS='user.ktakeda:user.ktakeda.HITS.v12_EXT0'\
  --outDS='user.ktakeda.Reco.v6'\
  --nJobs=50\
   --nEventsPerJob=200\
  --nFilesPerJob=1\
  --useNewTRF

# IDTRKVALIDStream.AddItem(\"xAOD::TauTrackContainer#TauTracks\"); IDTRKVALIDStream.AddItem(\"xAOD::TauTrackAuxContainer#TauTracksAux.\"); 
