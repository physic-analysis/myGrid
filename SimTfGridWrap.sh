#!/bin/bash

# setupATLAS ; lsetup git python panda ; asetup 21.0.68,Athena

#inputDS='user.ktakeda.MC.singletau.100000event.EVNT'
inputDS='user.ktakeda.SingleParticle.tau_EXT0'
maxEvents='10000'

for iJobs in `seq 1`
do
  skipEvents=`expr \( $iJobs - 1 \) \* 1000 `
  outputDS="user.ktakeda.HITS.skipEvents${skipEvents}.seriesI"
  pathena --trf "Sim_tf.py --conditionsTag 'default:OFLCOND-MC16-SDR-14' --geometryVersion 'default:ATLAS-R2-2016-01-00-01' --physicsList 'FTFP_BERT_ATL' --truthStrategy 'MC15aPlus' --simulator 'MC12G4' --DBRelease 'all:current' --DataRunNumber '284500' --preInclude 'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py' --postInclude 'default:mcHitRDO.py,RecJobTransforms/UseFrontier.py' --maxEvents ${maxEvents} --skipEvents ${skipEvents} --inputEVNTFile '%IN' --outputHITSFile '%OUT.HITS.pool.root'" --inDS=${inputDS} --outDS=${outputDS} --noBuild --extFile=mcHitRDO.py --split=5 --nEventsPerJob=2000 --nFilesPerJob=1

#user.ktakeda.SingleTau.MCevgen.pool.root

done
