#!/bin/sh

maxEvents=100000
jobConfig="/afs/cern.ch/user/k/ktakeda/workspace/public/physic-analysis/myGrid/single_tau.py"

Generate_trf.py \
  maxEvents=${maxEvents} \
  jobConfig=${jobConfig} \
  firstEvent=1 \
  randomSeed=12345 \
  runNumber=1 \
  outputEVNTFile=test.evgen \
  ecmEnergy=13000
