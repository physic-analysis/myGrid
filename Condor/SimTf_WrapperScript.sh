#!/bin/bash

# /////////////////////////////////////////////////////////////////////
#
# This script needs an argument regarding to the number of skip events.
# When you use HTCondor, you can specify the argument via ~.sub file.
#
# /////////////////////////////////////////////////////////////////////

#  You will need this bit for every Athena job
# non-interactive shell doesn't do aliases by default. Set it so it does
shopt -s expand_aliases

# set up the aliases.
# note that ATLAS_LOCAL_ROOT_BASE (unlike the aliases) is passed the shell from where you are submitting.
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

# now proceed normally to set up the other aliases
setupATLAS
asetup 21.0.68,Athena

# Each parameter will be defined.
skipEvents=$1
simulator="MC12G4"
inputEVNTFile='/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/SingleParticle/Data/EVGEN/SingleTau_100000events.evgen'
postUserInclude='/afs/cern.ch/user/k/ktakeda/workspace/public/physic-analysis/myGrid/Condor/mcHitRDO.py'

outpuHITS="/afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/SingleParticle/Data/HITS/${simulator}/100000events/user.ktakeda.${simulator}.skippEvents${skipEvents}.HITS.pool.root"

Sim_tf.py \
  --conditionsTag 'default:OFLCOND-MC16-SDR-14' \
  --geometryVersion 'default:ATLAS-R2-2016-01-00-01' \
  --physicsList 'FTFP_BERT_ATL' \
  --truthStrategy 'MC15aPlus' \
  --simulator ${simulator} \
  --DBRelease 'all:current'\
  --DataRunNumber '284500' \
  --preInclude 'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py' \
  --postInclude "default:${postUserInclude},RecJobTransforms/UseFrontier.py" \
  --maxEvents '100' \
  --skipEvents ${skipEvents} \
  --inputEVNTFile ${inputEVNTFile} \
  --outputHITSFile ${outpuHITS}
